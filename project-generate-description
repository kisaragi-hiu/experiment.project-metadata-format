#!/bin/bash
pwd="$(pwd)"
cd "$(dirname "${BASH_SOURCE[0]}")" || exit 1
source "project-retrieve"
cd "$pwd" || exit 1

get_prefix() {
	case "$1" in
	niconico)
		if [ "$platform" = "niconico" ]; then
			echo
		else
			echo "https://nico.ms/"
		fi
		;;
	youtube) echo "https://youtu.be/" ;;
	bowlroll) echo "https://bowlroll.net/file/" ;;
	*) echo ;;
	esac
}

title() {
	echo $(retrieve ".title") / $(retrieve ".voicebank.$lang")【UTAU "$_cover"】
}

echoexit() {
	local code="$1"
	shift 1
	echo "$@"
	exit "$code"
}

show_help() {
	echoexit "$1" \
		"$(basename "$0") [options]
    Options:
    -t  title mode
    -l  specify language
    -p  specify target platform
    -u  specify ust platform
    -o  specify original platform
    -h  show help (this message)
"
}

[ -f project.edn ] || echoexit 1 "project.edn not found"

# platform: youtube|niconico|lbry
# language: en|tc|ja
platform=youtube
language=en
ust=none
original_platform="$platform"

while getopts "tp:l:u:o:h" opt; do
	case "$opt" in
	t) mode="title" ;;
	p) platform="$OPTARG" ;;
	l) language="$OPTARG" ;;
	u) ust="$OPTARG" ;;
	o) original_platform="$OPTARG" ;;
	h) show_help 0 ;;
	*) show_help 1 ;;
	esac
done

case "$language" in
en*)
	_original="Original"
	_vocal="Vocal"
	_tuning="Tuning"
	_kisaragi="Kisaragi Hiu"
	_cover="Cover"
	lang=en
	;;
tc*)
	_original="本家"
	_vocal="歌聲"
	_tuning="調音"
	_kisaragi="如月飛羽"
	_cover="翻唱"
	lang=tc
	;;
ja*)
	_original="本家"
	_vocal="歌"
	_tuning="調声"
	_kisaragi="如月飛羽"
	_cover="カバー"
	lang=ja
	;;
esac

if [[ "$mode" == "title" ]]; then
	title
	exit
fi

retrieve ".publish.description.$lang"
echo "» $_original: $(retrieve ".credits.original.by" -c)"
echo "$(get_prefix "$original_platform")$(retrieve ".credits.original.link.$original_platform")"
echo "» $_vocal: $(retrieve ".voicebank.$lang")"

if [ "$ust" != "none" ]; then
	echo "» UST: $(retrieve ".credits.ust.by")"
	echo "$(get_prefix "$ust")$(retrieve ".credits.ust.link.$ust")"
fi

if [ "$platform" = "niconico" ] || [ "$platform" = "lbry" ]; then
	echo
	echo "» Youtube"
	echo "https://youtu.be/$(retrieve ".publish.link.youtube")"
fi
if [ "$platform" = "youtube" ] || [ "$platform" = "lbry" ]; then
	echo
	echo "» niconico"
	echo "https://nico.ms/$(retrieve ".publish.link.niconico")"
fi
